// To parse this JSON data, do
//
//     final vehiculos = vehiculosFromJson(jsonString);

import 'dart:convert';

Vehiculos vehiculosFromJson(String str) => Vehiculos.fromJson(json.decode(str));

String vehiculosToJson(Vehiculos data) => json.encode(data.toJson());

class Vehiculos {
  Vehiculos({
    this.ok,
    this.vehiculo,
  });

  bool ok;
  List<Vehiculo> vehiculo;

  factory Vehiculos.fromJson(Map<String, dynamic> json) => Vehiculos(
        ok: json["ok"],
        vehiculo: List<Vehiculo>.from(
            json["vehiculo"].map((x) => Vehiculo.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "ok": ok,
        "vehiculo": List<dynamic>.from(vehiculo.map((x) => x.toJson())),
      };
}

class Vehiculo {
  Vehiculo({
    this.telefono,
    this.rfc,
    this.modelo,
    this.tipo,
    this.marca,
    this.color,
    this.placa,
    this.nseries,
    this.nmotor,
    this.kmrecorridos,
    this.etapa,
    this.nombre,
    this.usuario,
    this.fechaIngreso,
    this.exteriores,
    this.interiores,
    this.accesorios,
    this.componentes,
    this.orderservicio,
    this.pdf,
    this.uid,
  });

  String telefono;
  String rfc;
  String modelo;
  String tipo;
  String marca;
  String color;
  String placa;
  String nseries;
  String nmotor;
  String kmrecorridos;
  int etapa;
  String nombre;
  Usuario usuario;
  DateTime fechaIngreso;
  Exteriores exteriores;
  Interiores interiores;
  Accesorios accesorios;
  Componentes componentes;
  Orderservicio orderservicio;
  PDF pdf;
  String uid;

  factory Vehiculo.fromJson(Map<String, dynamic> json) => Vehiculo(
        telefono: json["telefono"],
        rfc: json["rfc"],
        modelo: json["modelo"],
        tipo: json["tipo"],
        marca: json["marca"],
        color: json["color"],
        placa: json["placa"],
        nseries: json["nseries"],
        nmotor: json["nmotor"],
        kmrecorridos: json["kmrecorridos"],
        etapa: json["etapa"],
        nombre: json["nombre"],
        usuario: Usuario.fromJson(json["usuario"]),
        fechaIngreso: DateTime.parse(json["fechaIngreso"]),
        exteriores: Exteriores.fromJson(json["exteriores"]),
        interiores: Interiores.fromJson(json["interiores"]),
        accesorios: Accesorios.fromJson(json["accesorios"]),
        componentes: Componentes.fromJson(json["componentes"]),
        orderservicio: Orderservicio.fromJson(json["orderservicio"]),
        pdf: PDF.fromJson(json["pdf"]),
        uid: json["uid"],
      );

  Map<String, dynamic> toJson() => {
        "telefono": telefono,
        "rfc": rfc,
        "modelo": modelo,
        "tipo": tipo,
        "marca": marca,
        "color": color,
        "placa": placa,
        "nseries": nseries,
        "nmotor": nmotor,
        "kmrecorridos": kmrecorridos,
        "etapa": etapa,
        "nombre": nombre,
        "usuario": usuario.toJson(),
        "fechaIngreso": fechaIngreso.toIso8601String(),
        "exteriores": exteriores.toJson(),
        "interiores": interiores.toJson(),
        "accesorios": accesorios.toJson(),
        "componentes": componentes.toJson(),
        "orderservicio": orderservicio.toJson(),
        "pdf": pdf.toJson(),
        "uid": uid,
      };
}

class Accesorios {
  Accesorios({
    this.gato,
    this.maneralGato,
    this.llaveRuedas,
    this.estucheHerramientas,
    this.trianguloSeguridad,
    this.llantaRefaccion,
    this.extinguidor,
    this.id,
    this.v,
  });

  bool gato;
  bool maneralGato;
  bool llaveRuedas;
  bool estucheHerramientas;
  bool trianguloSeguridad;
  bool llantaRefaccion;
  bool extinguidor;
  String id;
  int v;

  factory Accesorios.fromJson(Map<String, dynamic> json) => Accesorios(
        gato: json["gato"],
        maneralGato: json["maneralGato"],
        llaveRuedas: json["llaveRuedas"],
        estucheHerramientas: json["estucheHerramientas"],
        trianguloSeguridad: json["trianguloSeguridad"],
        llantaRefaccion: json["llantaRefaccion"],
        extinguidor: json["extinguidor"],
        id: json["_id"],
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "gato": gato,
        "maneralGato": maneralGato,
        "llaveRuedas": llaveRuedas,
        "estucheHerramientas": estucheHerramientas,
        "trianguloSeguridad": trianguloSeguridad,
        "llantaRefaccion": llantaRefaccion,
        "extinguidor": extinguidor,
        "_id": id,
        "__v": v,
      };
}

class Componentes {
  Componentes({
    this.claxon,
    this.taponAceite,
    this.taponRadiador,
    this.varillaAceite,
    this.catalizador,
    this.bateria,
    this.id,
    this.v,
  });

  bool claxon;
  bool taponAceite;
  bool taponRadiador;
  bool varillaAceite;
  bool catalizador;
  bool bateria;
  String id;
  int v;

  factory Componentes.fromJson(Map<String, dynamic> json) => Componentes(
        claxon: json["claxon"],
        taponAceite: json["taponAceite"],
        taponRadiador: json["taponRadiador"],
        varillaAceite: json["varillaAceite"],
        catalizador: json["catalizador"],
        bateria: json["bateria"],
        id: json["_id"],
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "claxon": claxon,
        "taponAceite": taponAceite,
        "taponRadiador": taponRadiador,
        "varillaAceite": varillaAceite,
        "catalizador": catalizador,
        "bateria": bateria,
        "_id": id,
        "__v": v,
      };
}

class Exteriores {
  Exteriores({
    this.uluces,
    this.cuartosluces,
    this.antena,
    this.espejoLateral,
    this.cristales,
    this.emblema,
    this.llantas,
    this.taponRuedas,
    this.molduras,
    this.taponGasolina,
    this.carroceriaGolpes,
    this.bocinasClaxon,
    this.limpiadores,
    this.id,
    this.v,
  });

  bool uluces;
  bool cuartosluces;
  bool antena;
  bool espejoLateral;
  bool cristales;
  bool emblema;
  bool llantas;
  bool taponRuedas;
  bool molduras;
  bool taponGasolina;
  bool carroceriaGolpes;
  bool bocinasClaxon;
  bool limpiadores;
  String id;
  int v;

  factory Exteriores.fromJson(Map<String, dynamic> json) => Exteriores(
        uluces: json["uluces"],
        cuartosluces: json["cuartosluces"],
        antena: json["antena"],
        espejoLateral: json["espejoLateral"],
        cristales: json["cristales"],
        emblema: json["emblema"],
        llantas: json["llantas"],
        taponRuedas: json["taponRuedas"],
        molduras: json["molduras"],
        taponGasolina: json["taponGasolina"],
        carroceriaGolpes: json["carroceriaGolpes"],
        bocinasClaxon: json["bocinasClaxon"],
        limpiadores: json["limpiadores"],
        id: json["_id"],
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "uluces": uluces,
        "cuartosluces": cuartosluces,
        "antena": antena,
        "espejoLateral": espejoLateral,
        "cristales": cristales,
        "emblema": emblema,
        "llantas": llantas,
        "taponRuedas": taponRuedas,
        "molduras": molduras,
        "taponGasolina": taponGasolina,
        "carroceriaGolpes": carroceriaGolpes,
        "bocinasClaxon": bocinasClaxon,
        "limpiadores": limpiadores,
        "_id": id,
        "__v": v,
      };
}

class Interiores {
  Interiores({
    this.itablero,
    this.calefaccion,
    this.radio,
    this.bocinas,
    this.encendedor,
    this.espejoRetrovisor,
    this.ceniceros,
    this.cinturones,
    this.botonesInteriores,
    this.manijasInteriores,
    this.tapetes,
    this.vestidura,
    this.id,
    this.v,
  });

  bool itablero;
  bool calefaccion;
  bool radio;
  bool bocinas;
  bool encendedor;
  bool espejoRetrovisor;
  bool ceniceros;
  bool cinturones;
  bool botonesInteriores;
  bool manijasInteriores;
  bool tapetes;
  bool vestidura;
  String id;
  int v;

  factory Interiores.fromJson(Map<String, dynamic> json) => Interiores(
        itablero: json["itablero"],
        calefaccion: json["calefaccion"],
        radio: json["radio"],
        bocinas: json["bocinas"],
        encendedor: json["encendedor"],
        espejoRetrovisor: json["espejoRetrovisor"],
        ceniceros: json["ceniceros"],
        cinturones: json["cinturones"],
        botonesInteriores: json["botonesInteriores"],
        manijasInteriores: json["manijasInteriores"],
        tapetes: json["tapetes"],
        vestidura: json["vestidura"],
        id: json["_id"],
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "itablero": itablero,
        "calefaccion": calefaccion,
        "radio": radio,
        "bocinas": bocinas,
        "encendedor": encendedor,
        "espejoRetrovisor": espejoRetrovisor,
        "ceniceros": ceniceros,
        "cinturones": cinturones,
        "botonesInteriores": botonesInteriores,
        "manijasInteriores": manijasInteriores,
        "tapetes": tapetes,
        "vestidura": vestidura,
        "_id": id,
        "__v": v,
      };
}

class Orderservicio {
  Orderservicio({
    this.fecha,
    this.trabajoRealizar,
    this.diagnostico,
    this.riesgos,
  });

  DateTime fecha;
  String trabajoRealizar;
  String diagnostico;
  String riesgos;

  factory Orderservicio.fromJson(Map<String, dynamic> json) => Orderservicio(
      fecha: DateTime.parse(json["fecha"]),
      trabajoRealizar: json["trabajoRealizar"],
      diagnostico: json["diagnostico"],
      riesgos: json["riesgos"]);

  Map<String, dynamic> toJson() => {
        "fecha": fecha.toIso8601String(),
        "trabajoRealizar": trabajoRealizar,
        "diagnostico": diagnostico,
        "riesgos": riesgos,
      };
}

class Usuario {
  Usuario({
    this.online,
    this.name,
    this.email,
    this.password,
    this.sucursal,
  });

  bool online;
  String name;
  String email;
  String password;
  Sucursal sucursal;

  factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
      online: json["online"],
      name: json["name"],
      email: json["email"],
      password: json["password"],
      sucursal: Sucursal.fromJson(json["sucursal"]));

  Map<String, dynamic> toJson() => {
        "online": online,
        "name": name,
        "email": email,
        "password": password,
        "sucursal": sucursal.toJson(),
      };
}

class Sucursal {
  Sucursal(
      {this.descripcion, this.ciudad, this.estado, this.status, this.nombre});

  String descripcion;
  String ciudad;
  String estado;
  String status;
  String nombre;

  factory Sucursal.fromJson(Map<String, dynamic> json) => Sucursal(
      descripcion: json["descripcion"],
      ciudad: json["ciudad"],
      estado: json["estado"],
      status: json["status"],
      nombre: json["nombre"]);

  Map<String, dynamic> toJson() => {
        "descripcion": descripcion,
        "ciudad": ciudad,
        "estado": estado,
        "status": status,
        "nombre": nombre
      };
}

class PDF {
  PDF(
      {this.urlsalida,
      this.urlentrada,
      this.creatAt,
      this.id,
      this.servicio,
      this.observacion});

  String urlsalida;
  String urlentrada;
  String creatAt;
  String id;
  String servicio;
  String observacion;

  factory PDF.fromJson(Map<String, dynamic> json) => PDF(
      urlsalida: json['urlsalida'],
      urlentrada: json['urlentrada'],
      creatAt: json['creatAt'],
      id: json['_id'],
      servicio: json['servicio'],
      observacion: json['observacion']);

  Map<String, dynamic> toJson() => {
        "urlsalida": urlsalida,
        "urlentrada": urlentrada,
        "creatAt": creatAt,
        "_id": id,
        "servicio": servicio,
        "observacion": observacion
      };
}
