// To parse this JSON data, do
//
//     final sucursales = sucursalesFromJson(jsonString);

import 'dart:convert';

Sucursales sucursalesFromJson(String str) =>
    Sucursales.fromJson(json.decode(str));

String sucursalesToJson(Sucursales data) => json.encode(data.toJson());

class Sucursales {
  Sucursales({
    this.ok,
    this.sucursal,
  });

  bool ok;
  List<Sucursal> sucursal;

  factory Sucursales.fromJson(Map<String, dynamic> json) => Sucursales(
        ok: json["ok"],
        sucursal: List<Sucursal>.from(
            json["sucursales"].map((x) => Sucursal.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "ok": ok,
        "sucursales": List<dynamic>.from(sucursal.map((x) => x.toJson())),
      };
}

class Sucursal {
  Sucursal({
    this.descripcion,
    this.ciudad,
    this.estado,
    this.status,
    this.nombre,
    this.uid,
  });

  String descripcion;
  String ciudad;
  String estado;
  String status;
  String nombre;
  String uid;

  factory Sucursal.fromJson(Map<String, dynamic> json) => Sucursal(
        descripcion: json["descripcion"],
        ciudad: json["ciudad"],
        estado: json["estado"],
        status: json["status"],
        nombre: json["nombre"],
        uid: json["uid"],
      );

  Map<String, dynamic> toJson() => {
        "descripcion": descripcion,
        "ciudad": ciudad,
        "estado": estado,
        "status": status,
        "nombre": nombre,
        "uid": uid,
      };
}
