import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';

import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';
import 'package:chat/helpers/mostrarAlertas.dart';
import 'package:chat/models/vehiculo.dart';
import 'package:chat/services/vehiculos_services.dart';
import 'package:chat/services/upload_service.dart';

import 'list_vehiculos_historial.dart';

class SignaturePage extends StatefulWidget {
  const SignaturePage({
    Key key,
  }) : super(key: key);

  @override
  _SignaturePageState createState() => _SignaturePageState();
}

class _SignaturePageState extends State<SignaturePage> {
  final keySignaturePad = GlobalKey<SfSignaturePadState>();
  final keySignaturePadCliente = GlobalKey<SfSignaturePadState>();
  final vehiculosServices = new GetVehiculos();
  final upload = new Upload();

  @override
  Widget build(BuildContext context) {
    VehiculoUID args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(148, 19, 5, 1),
        title: Text('Firmar Orden de Entrada',
            style: TextStyle(color: Color.fromRGBO(3, 3, 3, 1))),
        actions: [
          TextButton(
            onPressed: () async {
              keySignaturePadCliente.currentState.clear();
            },
            child: Icon(Icons.brush_sharp),
            style: TextButton.styleFrom(primary: Color.fromRGBO(3, 3, 3, 1)),
          )
        ],
      ),
      body: Container(
        color: Color.fromRGBO(116, 125, 144, 1),
        child: Column(
          children: [
            SfSignaturePad(
              key: keySignaturePadCliente,
              backgroundColor: Colors.white.withOpacity(0.2),
            ),
            Divider()
          ],
        ),
      ),
      bottomNavigationBar: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Color.fromRGBO(148, 19, 5, 1),
          minimumSize: Size(double.infinity, 50),
        ),
        child: Text('Guardar firma',
            style: TextStyle(color: Color.fromRGBO(3, 3, 3, 1))),
        onPressed: () => {onSubmit(args.uid)},
      ),
    );
  }

  Future onSubmit(uid) async {
    try {
      final imageCliente = await keySignaturePadCliente.currentState?.toImage();

      ByteData imageSignatureCliente =
          await imageCliente.toByteData(format: ui.ImageByteFormat.png);

      final img64 = base64.encode(imageSignatureCliente.buffer.asUint8List());
      await upload.setUploadFile(uid, img64, '', '', '');

      Navigator.pushReplacement(context,
          PageRouteBuilder(pageBuilder: (_, __, ___) => HistorialVehiculo()));
    } catch (e) {
      mostrarAlertas(
          context, "Error", 'Escriba la firma antes de continuar $e');
    }
  }
}
