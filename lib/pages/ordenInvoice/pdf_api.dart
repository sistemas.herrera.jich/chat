import 'dart:io';
import 'dart:typed_data';

import 'package:chat/models/vehiculos.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class PdfApi {
  static Future<File> generatePDF(
      {ByteData logo,
      //ByteData imageSignature,
      ByteData imageSignatureCliente,
      List<Vehiculo> vehiculo,
      ByteData diagrama}) async {
    final document = PdfDocument();
    final page = document.pages.add();
    final contractoPage = document.pages.add();
    drawHeader(page, logo, vehiculo);

    drawDiagrama(page, diagrama);
    //drawSignature(page, imageSignature);
    drawSignatureCliente(page, imageSignatureCliente);
    drawListView(page, vehiculo);
    drawListViewDos(page, vehiculo);
    drawFooter(page, vehiculo);
    drawPen(page, 166);
    drawPen(page, 180);
    drawPen(page, 120);

    //Page two
    drawTextAndSignature(contractoPage, imageSignatureCliente, logo);
    return saveFile(document);
  }

  static Future<File> saveFile(PdfDocument document) async {
    final path = await getApplicationDocumentsDirectory();
    final fileName =
        path.path + '/Orden${DateTime.now().toIso8601String()}.pdf';
    final file = File(fileName);
    file.writeAsBytes(document.save());
    document.dispose();
    return file;
  }

  static PdfLayoutResult drawHeader(
      PdfPage page, ByteData logo, List<Vehiculo> vehiculo) {
    final pageSize = page.getClientSize();
    final PdfBitmap image = PdfBitmap(logo.buffer.asUint8List());

    String numOrden = vehiculo[0].uid;
    String lugar = vehiculo[0].usuario.sucursal.ciudad +
        " " +
        vehiculo[0].usuario.sucursal.estado +
        " ";
    String fechaEntrada = vehiculo[0].orderservicio.fecha.toString();
    String nombre = vehiculo[0].nombre;
    String rfc = vehiculo[0].rfc;
    String modelo = vehiculo[0].modelo;
    String marca = vehiculo[0].marca;
    String nseries = vehiculo[0].nseries;
    String nmotor = vehiculo[0].nmotor;
    String placa = vehiculo[0].placa;
    String tipo = vehiculo[0].tipo;
    String color = vehiculo[0].color;
    String telefono = vehiculo[0].telefono;
    String kmrecorridos = vehiculo[0].kmrecorridos;

    page.graphics.drawImage(
      image,
      Rect.fromLTWH(0, 0, pageSize.width - 350, 90),
    );

    final encabezado = 'REPARACIÓN Y/O MANTENIMIENTO DE VEHÍCULOS';
    page.graphics.drawString(
      encabezado,
      PdfStandardFont(PdfFontFamily.helvetica, 12),
      bounds: Rect.fromLTWH(200, 0, 300, 50),
      brush: PdfBrushes.black,
      format: PdfStringFormat(
        alignment: PdfTextAlignment.center,
        lineAlignment: PdfVerticalAlignment.middle,
      ),
    );

    final contentFont = PdfStandardFont(PdfFontFamily.helvetica, 9);

    page.graphics.drawString(
      'Orden de servicio No. : $numOrden \n\r Lugar y Fecha: $lugar $fechaEntrada',
      contentFont,
      brush: PdfBrushes.black,
      bounds: Rect.fromLTWH(200, 40, 300, 33),
      format: PdfStringFormat(
        alignment: PdfTextAlignment.left,
        lineAlignment: PdfVerticalAlignment.bottom,
      ),
    );

    page.graphics.drawString(
      'JHK MULTISERVICIOS SA DE CV DOMICILIO: BLVD. FRANCISCO I. MADERO #2151 COL. MIGUEL HIDALGO CULIACAN, SINALOA R.F.C.: JMU-15031-LX7',
      contentFont,
      brush: PdfBrushes.black,
      bounds: Rect.fromLTWH(0, 75, 200, 55),
      format: PdfStringFormat(
        alignment: PdfTextAlignment.left,
        lineAlignment: PdfVerticalAlignment.bottom,
      ),
    );

    final contentFontDatosVehiculo =
        PdfStandardFont(PdfFontFamily.helvetica, 12);

    final text = '''NOMBRE: $nombre ''';
    PdfTextElement(text: text, font: contentFontDatosVehiculo).draw(
        page: page,
        bounds: Rect.fromLTWH(0, 150, page.getClientSize().width, 30));

    final dataGridInfo = PdfGrid();
    dataGridInfo.columns.add(count: 5);
    final headerRowVehiculo = dataGridInfo.headers.add(1)[0];
    headerRowVehiculo.style.backgroundBrush =
        PdfSolidBrush(PdfColor(165, 165, 165));
    headerRowVehiculo.style.textBrush = PdfBrushes.white;
    headerRowVehiculo.cells[0].value = 'TELEFONO';
    headerRowVehiculo.cells[1].value = 'RFC';
    headerRowVehiculo.cells[2].value = 'MODELO';
    headerRowVehiculo.cells[3].value = 'TIPO';
    headerRowVehiculo.cells[4].value = 'MARCA';
    headerRowVehiculo.style.font =
        PdfStandardFont(PdfFontFamily.helvetica, 9, style: PdfFontStyle.bold);
    final row = dataGridInfo.rows.add();

    row.cells[0].value = telefono;
    row.cells[1].value = rfc;
    row.cells[2].value = modelo;
    row.cells[3].value = tipo;
    row.cells[4].value = marca;

    dataGridInfo.applyBuiltInStyle(PdfGridBuiltInStyle.gridTable1Light);

    for (int i = 0; i < headerRowVehiculo.cells.count; i++) {
      headerRowVehiculo.cells[i].style.cellPadding =
          PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
    }

    for (int i = 0; i < dataGridInfo.rows.count; i++) {
      final row = dataGridInfo.rows[i];
      for (int j = 0; j < row.cells.count; j++) {
        final cell = row.cells[j];

        cell.style.cellPadding =
            PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
      }
    }

    dataGridInfo.draw(
      page: page,
      bounds: Rect.fromLTWH(0, 180, 0, 0),
    );

    final dataGridInfo2 = PdfGrid();
    dataGridInfo2.columns.add(count: 5);
    final headerRow = dataGridInfo2.headers.add(1)[0];
    headerRow.style.backgroundBrush = PdfSolidBrush(PdfColor(165, 165, 165));
    headerRow.style.textBrush = PdfBrushes.white;
    headerRow.cells[0].value = 'COLOR';
    headerRow.cells[1].value = 'PLACAS';
    headerRow.cells[2].value = 'N° SERIE';
    headerRow.cells[3].value = 'N° DE MOTOR';
    headerRow.cells[4].value = 'KM RECORRIDOS';
    headerRow.style.font =
        PdfStandardFont(PdfFontFamily.helvetica, 9, style: PdfFontStyle.bold);
    final row2 = dataGridInfo2.rows.add();

    row2.cells[0].value = color;
    row2.cells[1].value = placa;
    row2.cells[2].value = nseries;
    row2.cells[3].value = nmotor;
    row2.cells[4].value = kmrecorridos;

    dataGridInfo2.applyBuiltInStyle(PdfGridBuiltInStyle.gridTable1Light);

    for (int i = 0; i < headerRow.cells.count; i++) {
      headerRow.cells[i].style.cellPadding =
          PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
    }

    for (int i = 0; i < dataGridInfo2.rows.count; i++) {
      final row = dataGridInfo2.rows[i];
      for (int j = 0; j < row.cells.count; j++) {
        final cell = row.cells[j];

        cell.style.cellPadding =
            PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
      }
    }

    dataGridInfo2.draw(
      page: page,
      bounds: Rect.fromLTWH(0, 230, 0, 0),
    );

    return PdfTextElement(
      format: PdfStringFormat(lineSpacing: 0),
    ).draw(
      page: page,
      bounds: Rect.fromLTWH(
        10,
        400,
        pageSize.width / 3,
        pageSize.height - 100,
      ),
    );
  }

  static void drawDiagrama(PdfPage page, ByteData diagrama) {
    final pageSize = page.getClientSize();
    final PdfBitmap image = PdfBitmap(diagrama.buffer.asUint8List());
    page.graphics.drawImage(
      image,
      Rect.fromLTWH(pageSize.width / 3, 280.0 + 30, pageSize.width / 3, 150),
    );
  }

  static void drawListView(PdfPage page, List<Vehiculo> vehiculo) {
    final pageSize = page.getClientSize();
    final double top = 280;
    final contentFont = PdfStandardFont(PdfFontFamily.helvetica, 9);
    final String antena = vehiculo[0].exteriores.antena == true ? "SI" : "NO";
    final String uluces = vehiculo[0].exteriores.uluces == true ? "SI" : "NO";
    final String cuartosluces =
        vehiculo[0].exteriores.cuartosluces == true ? "SI" : "NO";
    final String espejoLateral =
        vehiculo[0].exteriores.espejoLateral == true ? "SI" : "NO";
    final String cristales =
        vehiculo[0].exteriores.cristales == true ? "SI" : "NO";
    final String emblema = vehiculo[0].exteriores.emblema == true ? "SI" : "NO";
    final String llantas = vehiculo[0].exteriores.llantas == true ? "SI" : "NO";
    final String taponRuedas =
        vehiculo[0].exteriores.taponRuedas == true ? "SI" : "NO";
    final String molduras =
        vehiculo[0].exteriores.molduras == true ? "SI" : "NO";
    final String taponGasolina =
        vehiculo[0].exteriores.taponGasolina == true ? "SI" : "NO";
    final String carroceriaGolpes =
        vehiculo[0].exteriores.carroceriaGolpes == true ? "SI" : "NO";
    final String bocinasClaxon =
        vehiculo[0].exteriores.bocinasClaxon == true ? "SI" : "NO";
    final String limpiadores =
        vehiculo[0].exteriores.limpiadores == true ? "SI" : "NO";

    final String itablero =
        vehiculo[0].interiores.itablero == true ? "SI" : "NO";
    final String calefaccion =
        vehiculo[0].interiores.calefaccion == true ? "SI" : "NO";
    final String radio = vehiculo[0].interiores.radio == true ? "SI" : "NO";
    final String bocinas = vehiculo[0].interiores.bocinas == true ? "SI" : "NO";
    final String encendedor =
        vehiculo[0].interiores.encendedor == true ? "SI" : "NO";
    final String espejoRetrovisor =
        vehiculo[0].interiores.espejoRetrovisor == true ? "SI" : "NO";
    final String ceniceros =
        vehiculo[0].interiores.ceniceros == true ? "SI" : "NO";
    final String cinturones =
        vehiculo[0].interiores.cinturones == true ? "SI" : "NO";
    final String botonesInteriores =
        vehiculo[0].interiores.botonesInteriores == true ? "SI" : "NO";
    final String manijasInteriores =
        vehiculo[0].interiores.manijasInteriores == true ? "SI" : "NO";
    final String tapetes = vehiculo[0].interiores.tapetes == true ? "SI" : "NO";
    final String vestidura =
        vehiculo[0].interiores.vestidura == true ? "SI" : "NO";

    page.graphics.drawRectangle(
        brush: PdfBrushes.gray,
        bounds: Rect.fromLTWH(0, top, pageSize.width / 3, 30));

    page.graphics.drawString(
      'EXTERIORES',
      PdfStandardFont(PdfFontFamily.helvetica, 10),
      brush: PdfBrushes.white,
      bounds: Rect.fromLTWH(40, top, 100, 30),
      format: PdfStringFormat(
          alignment: PdfTextAlignment.center,
          lineAlignment: PdfVerticalAlignment.middle),
    );
    PdfOrderedList(
            items: PdfListItemCollection(<String>[
              'UNIDAD DE LUCES',
              '1/4 LUCES',
              'ANTENA',
              'ESPEJO LATERAL',
              'CRISTALES',
              'EMBLEMA',
              'LLANTAS (4)',
              'TAPON DE RUEDAS (4)',
              'MOLDURAS COMPLETAS',
              'TAPON DE GASOLINA',
              'CARROCERÍA SIN GOLPES',
              'BOCINAS DE CLAXON',
              'LIMPIADORES (PLUMAS)'
            ]),
            font: contentFont,
            indent: 0,
            format: PdfStringFormat(lineSpacing: 1))
        .draw(
            page: page,
            bounds: Rect.fromLTWH(0, top + 30, pageSize.width / 3 - 30, 0));

    PdfOrderedList(
            style: PdfNumberStyle.none,
            items: PdfListItemCollection(<String>[
              uluces,
              cuartosluces,
              antena,
              espejoLateral,
              cristales,
              emblema,
              llantas,
              taponRuedas,
              molduras,
              taponGasolina,
              carroceriaGolpes,
              bocinasClaxon,
              limpiadores
            ]),
            font: contentFont,
            indent: 0,
            format: PdfStringFormat(lineSpacing: 1))
        .draw(
            page: page,
            bounds: Rect.fromLTWH(pageSize.width / 3 - 30, top + 30, 40, 0));

    page.graphics.drawRectangle(
        brush: PdfBrushes.gray,
        bounds: Rect.fromLTWH(pageSize.width / 3, top, pageSize.width / 3, 30));

    page.graphics.drawRectangle(
        brush: PdfBrushes.gray,
        bounds:
            Rect.fromLTWH(pageSize.width / 3 * 2, top, pageSize.width / 3, 30));

    page.graphics.drawString(
      'INTERIORES',
      PdfStandardFont(PdfFontFamily.helvetica, 10),
      brush: PdfBrushes.white,
      bounds: Rect.fromLTWH(pageSize.width / 3 * 2 + 40, top, 100, 30),
      format: PdfStringFormat(
          alignment: PdfTextAlignment.center,
          lineAlignment: PdfVerticalAlignment.middle),
    );
    PdfOrderedList(
            items: PdfListItemCollection(<String>[
              'INSTRUMENTOS DE TABLERO',
              'CALEFACCIÓN',
              'RADIO/TIPO',
              'BOCINAS',
              'ENCENDEDOR',
              'ESPEJO RETROVISOR',
              'CENICEROS',
              'CINTURONES',
              'BOTONES DE INTERIORES',
              'MANIJAS DE INTERIORES',
              'TAPETES',
              'VESTIDURAS'
            ]),
            font: contentFont,
            indent: 0,
            format: PdfStringFormat(lineSpacing: 1))
        .draw(
            page: page,
            bounds: Rect.fromLTWH(
                pageSize.width / 3 * 2, top + 30, pageSize.width / 3 - 30, 0));
    PdfOrderedList(
            style: PdfNumberStyle.none,
            items: PdfListItemCollection(<String>[
              itablero,
              calefaccion,
              radio,
              bocinas,
              encendedor,
              espejoRetrovisor,
              ceniceros,
              cinturones,
              botonesInteriores,
              manijasInteriores,
              tapetes,
              vestidura
            ]),
            font: contentFont,
            indent: 0,
            format: PdfStringFormat(lineSpacing: 1))
        .draw(page: page, bounds: Rect.fromLTWH(490, top + 30, 40, 0));
  }

  static void drawListViewDos(PdfPage page, List<Vehiculo> vehiculo) {
    final pageSize = page.getClientSize();
    final double top = 460;
    final contentFont = PdfStandardFont(PdfFontFamily.helvetica, 9);
    final String gato = vehiculo[0].accesorios.gato == true ? "SI" : "NO";
    final String maneralGato =
        vehiculo[0].accesorios.maneralGato == true ? "SI" : "NO";
    final String llaveRuedas =
        vehiculo[0].accesorios.llaveRuedas == true ? "SI" : "NO";
    final String estucheHerramientas =
        vehiculo[0].accesorios.estucheHerramientas == true ? "SI" : "NO";
    final String trianguloSeguridad =
        vehiculo[0].accesorios.trianguloSeguridad == true ? "SI" : "NO";
    final String llantaRefaccion =
        vehiculo[0].accesorios.llantaRefaccion == true ? "SI" : "NO";
    final String extinguidor =
        vehiculo[0].accesorios.extinguidor == true ? "SI" : "NO";

    final String claxon = vehiculo[0].componentes.claxon == true ? "SI" : "NO";
    final String taponAceite =
        vehiculo[0].componentes.taponAceite == true ? "SI" : "NO";
    final String taponRadiador =
        vehiculo[0].componentes.taponRadiador == true ? "SI" : "NO";
    final String varillaAceite =
        vehiculo[0].componentes.varillaAceite == true ? "SI" : "NO";
    final String catalizador =
        vehiculo[0].componentes.catalizador == true ? "SI" : "NO";
    final String bateria =
        vehiculo[0].componentes.bateria == true ? "SI" : "NO";

    page.graphics.drawRectangle(
        brush: PdfBrushes.gray,
        bounds: Rect.fromLTWH(0, top, pageSize.width / 3, 30));

    page.graphics.drawString(
      'ACCESORIOS',
      PdfStandardFont(PdfFontFamily.helvetica, 10),
      brush: PdfBrushes.white,
      bounds: Rect.fromLTWH(40, top, 100, 30),
      format: PdfStringFormat(
          alignment: PdfTextAlignment.center,
          lineAlignment: PdfVerticalAlignment.middle),
    );
    PdfOrderedList(
            items: PdfListItemCollection(<String>[
              'GATO',
              'MANERAL DE GATO',
              'LLAVE DE RUEDAS',
              'ESTUCHE DE HERRAMIENTAS',
              'TRIANGULO DE SEGURIDAD',
              'LLANTA DE REFACCIÓN',
              'EXTINGUIDOR',
            ]),
            font: contentFont,
            indent: 0,
            format: PdfStringFormat(lineSpacing: 1))
        .draw(
            page: page,
            bounds: Rect.fromLTWH(0, top + 30, pageSize.width / 3 - 30, 0));

    PdfOrderedList(
            style: PdfNumberStyle.none,
            items: PdfListItemCollection(<String>[
              gato,
              maneralGato,
              llaveRuedas,
              estucheHerramientas,
              trianguloSeguridad,
              llantaRefaccion,
              extinguidor
            ]),
            font: contentFont,
            indent: 0,
            format: PdfStringFormat(lineSpacing: 1))
        .draw(
            page: page,
            bounds: Rect.fromLTWH(pageSize.width / 3 - 30, top + 30, 40, 0));

    page.graphics.drawRectangle(
        brush: PdfBrushes.gray,
        bounds: Rect.fromLTWH(pageSize.width / 3, top, pageSize.width / 3, 30));

    page.graphics.drawRectangle(
        brush: PdfBrushes.gray,
        bounds:
            Rect.fromLTWH(pageSize.width / 3 * 2, top, pageSize.width / 3, 30));

    page.graphics.drawString(
      'COMPONENTES MECANICOS',
      PdfStandardFont(PdfFontFamily.helvetica, 10),
      brush: PdfBrushes.white,
      bounds: Rect.fromLTWH(pageSize.width / 3 * 2 + 40, top, 100, 30),
      format: PdfStringFormat(
          alignment: PdfTextAlignment.center,
          lineAlignment: PdfVerticalAlignment.middle),
    );
    PdfOrderedList(
            items: PdfListItemCollection(<String>[
              'CLAXON',
              'TAPON DE ACEITE',
              'TAPON DE RADIADOR',
              'VARILLA DE ACEITE',
              'CATALIZADOR',
              'BATERÍA (MCA)'
            ]),
            font: contentFont,
            indent: 0,
            format: PdfStringFormat(lineSpacing: 1))
        .draw(
            page: page,
            bounds: Rect.fromLTWH(
                pageSize.width / 3 * 2, top + 30, pageSize.width / 3 - 30, 0));
    PdfOrderedList(
            style: PdfNumberStyle.none,
            items: PdfListItemCollection(<String>[
              claxon,
              taponAceite,
              taponRadiador,
              varillaAceite,
              catalizador,
              bateria
            ]),
            font: contentFont,
            indent: 0,
            format: PdfStringFormat(lineSpacing: 1))
        .draw(page: page, bounds: Rect.fromLTWH(490, top + 30, 40, 0));
  }

  static void drawFooter(PdfPage page, List<Vehiculo> vehiculo) {
    final pageSize = page.getClientSize();
    final String trabajo = vehiculo[0].orderservicio.trabajoRealizar;
    final String diagnostico = vehiculo[0].orderservicio.diagnostico;
    final String riesgo = vehiculo[0].orderservicio.riesgos;
    final String fecha = vehiculo[0].orderservicio.fecha.toString();

    page.graphics.drawString(
        'ORDEN DE TRABAJO', PdfStandardFont(PdfFontFamily.helvetica, 12),
        bounds: Rect.fromLTWH(
            pageSize.width / 3 + 10, pageSize.height - 180, 150, 40));

    page.graphics.drawString(
        'OPERACIONES A EFECTUAR Y ELEMENTOS A REPARAR O SUSTITUIR',
        PdfStandardFont(PdfFontFamily.helvetica, 9),
        bounds: Rect.fromLTWH(
            pageSize.width / 6 + 5, pageSize.height - 160, 350, 20));
    page.graphics.drawString(
        trabajo, PdfStandardFont(PdfFontFamily.helvetica, 9),
        bounds: Rect.fromLTWH(
            10, pageSize.height - 140, pageSize.width - 100, 100));
    page.graphics.drawString(
        'EL CONSUMIDOR ACEPTA INVENTARIO EFECTUADO A LA UNIDAD CORRESPONDIENTE',
        PdfStandardFont(PdfFontFamily.helvetica, 9),
        bounds: Rect.fromLTWH(
            10, pageSize.height - 60, pageSize.width / 2 - 30, 40));
    PdfTextElement(
      format: PdfStringFormat(lineSpacing: 0),
    ).draw(
      page: page,
      bounds: Rect.fromLTWH(
        10,
        350,
        pageSize.width / 3,
        pageSize.height - 100,
      ),
    );

    page.graphics.drawString(
        vehiculo[0].usuario.name + '\n' + vehiculo[0].usuario.sucursal.nombre,
        PdfStandardFont(PdfFontFamily.helvetica, 9),
        bounds: Rect.fromLTWH(pageSize.width / 2, pageSize.height - 100,
            pageSize.width / 2, pageSize.height - 100));

    page.graphics.drawString(
        'EL USUARIO ACEPTA QUE SE REALIZO EL INVENTARIO MENCIONADO DE LA UNIDAD CORRESPONDIENTE',
        PdfStandardFont(PdfFontFamily.helvetica, 9),
        bounds: Rect.fromLTWH(
            pageSize.width / 2, pageSize.height - 60, pageSize.width / 2, 40));
  }

  static void drawSignature(PdfPage page, ByteData imageSignature) {
    final pageSize = page.getClientSize();
    final PdfBitmap image = PdfBitmap(imageSignature.buffer.asUint8List());

    page.graphics.drawImage(
      image,
      Rect.fromLTWH(400, pageSize.height - 100, 100, 40),
    );
  }

  static void drawSignatureCliente(PdfPage page, ByteData imageSignature) {
    final pageSize = page.getClientSize();
    final PdfBitmap image = PdfBitmap(imageSignature.buffer.asUint8List());

    page.graphics.drawImage(
      image,
      Rect.fromLTWH(20, pageSize.height - 100, 100, 40),
    );
  }

  static void drawPen(PdfPage page, double y) {
    final pageSize = page.getClientSize();
    final color = PdfColor(165, 165, 165);
    final linePen = PdfPen(color, dashStyle: PdfDashStyle.solid);
    final yLine = pageSize.height - y;

    page.graphics.drawLine(
      linePen,
      Offset(0, yLine),
      Offset(pageSize.width, yLine),
    );
  }

  static void drawPenX(PdfPage page, double y) {
    final pageSize = page.getClientSize();
    final color = PdfColor(165, 165, 165);
    final linePen = PdfPen(color, dashStyle: PdfDashStyle.solid);
    final yLine = pageSize.height - y;

    page.graphics.drawLine(
      linePen,
      Offset(0, yLine),
      Offset(pageSize.width, yLine),
    );
  }

  static void drawTextAndSignature(
      PdfPage page, ByteData imageSignature, ByteData logo) {
    final pageSize = page.getClientSize();
    final PdfBitmap image = PdfBitmap(imageSignature.buffer.asUint8List());
    final PdfBitmap imageLogo = PdfBitmap(logo.buffer.asUint8List());
    page.graphics.drawImage(
      imageLogo,
      Rect.fromLTWH(0, 0, pageSize.width - 350, 80),
    );

    var texhead =
        '''AUTORIZACIÓN PARA LA UTILIZACIÓN DE INFORMACIÓN CON FINES MERCADOTÉCNICOS O PUBLICITARIOS
EL CONSUMIDOR SI (X) NO (_) ACEPTA QUE EL PRESTADOR DEL SERVICIO CEDA O TRANSMITA A TERCEROS, CON FINES MERCADOTÉCNICOS O PUBLICITARIOS, LA INFORMACIÓN PROPORCIONADA POR ÉL CON MOTIVO DEL PRESENTE CONTRATO Y SI (X) NO (_) ACEPTA QUE EL PRESTADOR DEL SERVICIO LE ENVÍE PUBLICIDAD SOBRE BIENES Y SERVICIOS. 
''';
    page.graphics.drawString(
        texhead, PdfStandardFont(PdfFontFamily.helvetica, 7),
        bounds: Rect.fromLTWH(160, 20, 350, 200),
        format: PdfStringFormat(
            alignment: PdfTextAlignment.justify, paragraphIndent: 0));

    var text = '''FIRMA O RÚBRICA DE AUTORIZACIÓN DEL CONSUMIDOR:


CONTRATO DE PRESTACIÓN DE SERVICIOS DE REPARACIÓN Y/O MANTENIMIENTO DE VEHÍCULOS QUE CELEBRAN POR UNA PARTE JHK MULTISERVICIOS COMO “EL PRESTADOR DEL SERVICIO” Y POR LA OTRA PARTE COMO “EL CONSUMIDOR”, CUYOS NOMBRES Y DATOS CONSTAN EN LA CARÁTULA DE ESTE CONTRATO COMO PARTE INTEGRAL DEL MISMO, SUJETÁNDOSE AL TENOR DE LAS SIGUIENTES:

                                                                                                                        C L A U S U L A S

PRIMERA. - EL PRESTADOR DEL SERVICIO realizará todas las operaciones y composturas descritas en el anverso del presente contrato, solicitadas por EL CONSUMIDOR que suscribe el presente contrato, a las que se someterá el vehículo para obtener condiciones de funcionamiento de acuerdo con el estado de éste. Así mismo EL PRESTADOR DEL SERVICIO no condicionará la prestación de los servicios de reparación y/o mantenimiento de vehículos a la adquisición o renta de otros productos o servicios en el establecimiento o en otro taller o agencia predeterminada. 
SEGUNDA. - El precio total de los servicios contratados se establece en el presupuesto que forma parte del presente y se describe en el anverso del presente contrato, el cual será pagado por EL CONSUMIDOR, de la siguiente forma: Al momento de celebrar el presente contrato por concepto de anticipo la cantidad que se indica en el anverso del presente contrato y el resto en la fecha de entrega del vehículo. Todo pago efectuado por EL CONSUMIDOR deberá realizarse en el establecimiento de EL PRESTADOR DEL SERVICIO, al contado y en moneda nacional o cualquier moneda extranjera aceptada por EL PRESTADOR DEL SERVICIO, esto último deberá estar conforme a la Ley Monetaria de los Estados Unidos Mexicanos El pago será en efectivo, salvo que las partes acuerden o acepten otra forma distinta, como pudiese ser en cheque, tarjeta de crédito o depósito bancario. 
TERCERA. - EL PRESTADOR DEL SERVICIO pondrá a disposición de EL CONSUMIDOR los precios de los servicios, mano de obra, refacciones y materiales a usar en las reparaciones ofrecidas. Así mismo, previo a la realización del servicio EL PRESTADOR DEL SERVICIO presentará a EL CONSUMIDOR el presupuesto al que se refiere la cláusula segunda del presente contrato. Una vez aprobado el presupuesto por EL CONSUMIDOR, EL PRESTADOR DEL SERVICIO procederá a efectuar el servicio solicitado. Los incrementos que resulten durante la reparación por costos no previsible en rubros específicos que su cotización no indique estará fuera del control de EL PRESTADOR DEL SERVICIO, deberán ser autorizados por EL CONSUMIDOR de forma escrita, siempre y cuando éstos excedan al 20% del presupuesto. Si el incremento citado es inferior lo podrán autorizar telefónicamente. El tiempo, que, en su caso, transcurra para solicitar esta condición modificará la fecha de entrega, en la misma proporción. 
CUARTA. - La entrega del vehículo será en la fecha contemplada en el anverso del presente contrato. Para el caso de que EL CONSUMIDOR, sea el que proporcione las refacciones la fecha de entrega será
QUINTA. - EL PRESTADOR DEL SERVICIO exclusivamente utilizará para los servicios objetos, partes, refacciones u otros materiales nuevos y apropiados para el vehículo citados en este contrato, salvo que EL CONSUMIDOR autorice expresamente se usen otras. Si EL PRESTADOR DEL SERVICIO autoriza, EL CONSUMIDOR suministrará las partes, refacciones o materiales necesarios para la reparación y/o mantenimiento del vehículo. En ambos casos, la autorización respectiva se hará constar en el anverso de presente contrato. 
SEXTA. - EL PRESTADOR DEL SERVICIO hará entrega de las refacciones, partes o piezas sustituidas en la reparación y/o mantenimiento del vehículo cuando se solicite con anticipación al momento de generar el inventario de la unidad. 
SÉPTIMA. - Las reparaciones a que se refiere el presupuesto aceptado por EL CONSUMIDOR, tienen una garantía de 60 días contados a partir de la fecha de entrega del vehículo ya reparado en mano de obra y en refacciones la especificada por el fabricante, siempre y cuando no se manifieste mal uso, negligencia o descuido; lo anterior de conformidad a lo establecido en el artículo 77 de la Ley Federal de Protección al Consumidor. Si el vehículo es intervenido por un tercero, EL PRESTADOR DELSERVICIO no será responsable y la garantía quedará sin efecto. Las reclamaciones por garantía se harán en el establecimiento de EL PRESTADOR DEL SERVICIO, para lo cual EL CONSUMIDOR, deberá presentar su vehículo en dicho establecimiento. Las reparaciones efectuadas por EL PRESTADOR DEL SERVICIO en cumplimiento a la garantía del servicio serán sin cargo alguno para EL CONSUMIDOR, salvo aquellos trabajos que no deriven de las reparaciones aceptadas en el presupuesto. No se computará dentro del plazo de garantía, el tiempo que dure la reparación y/o mantenimiento del vehículo para el cumplimiento de la misma. Los gastos en que incurra EL CONSUMIDOR para hacer valida la garantía en un domicilio diverso al de EL PRESTADOR DEL SERVICIO, deberán ser cubiertos por éste. 
OCTAVA. - EL CONSUMIDOR, autoriza el uso del vehículo en zonas aledañas con un radio de 5 km. al área del establecimiento a efectos de pruebas o verificación de las reparaciones a efectuar o efectuadas. EL PRESTADOR DEL SERVICIO no podrá utilizar el vehículo para uso personal, fines propios o de terceros. 
NOVENA. - EL PRESTADOR DEL SERVICIO se hace responsable por los daños causados al vehículo de EL CONSUMIDOR, como consecuencia de los recorridos de prueba por parte del personal de EL PRESTADOR DEL SERVICIO. El riesgo en un recorrido de prueba es por cuenta de EL CONSUMIDOR, cuando él mismo solicite que será él o un representante suyo quién guíe el vehículo. Asimismo, EL PRESTADOR DEL SERVICIO se hace responsable por las descomposturas, daños, perdidas parciales o totales, imputables a él o a sus empleados, que sufra el vehículo, el equipo y aditamentos que EL CONSUMIDOR haya notificado al momento de la recepción del vehículo, mientras se encuentren bajo su responsabilidad para llevar a cabo la reparación y/o mantenimiento solicitado, así como para hacer efectiva la garantía otorgada. Para tal efecto EL PRESTADOR DEL SERVICIO si (X) no (_) cuenta con un seguro suficiente para cubrir dichas eventualidades, cuyo número de póliza es: __________________ con la compañía: ___________________________ EL PRESTADOR DEL SERVICIO no se hace responsable por la pérdida de objetos dejados en el interior del vehículo, aún con la cajuela cerrada, salvo que estos hayan sido notificados y puestos bajo su resguardo al momento de la recepción del vehículo. 
DÉCIMA. - EL PRESTADOR DEL SERVICIO se obliga a expedir la factura o comprobante de pago por los trabajos efectuados, en la que se especificará los precios por mano de obra, refacciones, materiales y accesorios empleados, conforme el artículo 62 de la Ley Federal de Protección al Consumidor. 
DÉCIMA PRIMERA. - Se establece como pena convencional por el incumplimiento de cualquiera de las partes a las obligaciones contraídas en el presente contrato, el 15% del precio total de la operación. 
DÉCIMA SEGUNDA. - En caso de que el vehículo no sea recogido por EL CONSUMIDOR en un plazo de 48 horas a partir de la fecha señalada para la entrega, pagará por concepto de depósito un salario mínimo vigente en el lugar que se celebre el presente contrato, por cada 24 hrs. que transcurran. 
DÉCIMA TERCERA. - EL CONSUMIDOR puede desistirse en cualquier momento de la contratación del servicio de reparación y/o mantenimiento del vehículo, en cuyo caso deberá cubrir, en lugar del precio contratado el importe de los trabajos realizados hasta el retiro del vehículo, incluidas las partes, refacciones u otros materiales utilizados. 
DÉCIMA CUARTA. - EL PRESTADOR DEL SERVICIO es responsable ante EL CONSUMIDOR por el incumplimiento de los servicios contratados, aún cuando subcontrate con terceros dicha prestación. 
DÉCIMA QUINTA. - Cuando se preste el servicio a domicilio, el personal de EL PRESTADOR DEL SERVICIO debe identificarse plenamente ante EL CONSUMIDOR, mediante la presentación del documento que lo acredite para este propósito. En caso de que dicho servicio tenga un costo, éste se indicará en el anverso del presente contrato. 
DÉCIMA SEXTA. - EL CONSUMIDOR libera a EL PRESTADOR DEL SERVICIO de cualquier responsabilidad que hubiere surgido o pudiese surgir con relación al origen, propiedad, posesión o cualquier otro derecho inherente al vehículo, partes o componentes de este. 

Leído que fue por las partes el contenido del presente contrato y sabedoras de su alcance legal, lo firman por duplicado en la Ciudad de __________ a los días ______ del mes ______ de del año _____.
    ''';

    page.graphics.drawString(text, PdfStandardFont(PdfFontFamily.helvetica, 7),
        bounds: Rect.fromLTWH(0, 80, pageSize.width, pageSize.height),
        format: PdfStringFormat(
            textDirection: PdfTextDirection.leftToRight,
            alignment: PdfTextAlignment.justify,
            paragraphIndent: 0));
    page.graphics.drawImage(
      image,
      Rect.fromLTWH(190, 65, 100, 40),
    );
    page.graphics.drawString('EL PRESTADOR DE SERVICIOS',
        PdfStandardFont(PdfFontFamily.helvetica, 7),
        bounds: Rect.fromLTWH(0, pageSize.height - 30, pageSize.width / 3, 20),
        format: PdfStringFormat(alignment: PdfTextAlignment.center));

    page.graphics.drawString(
        'EL COMSUMIDOR', PdfStandardFont(PdfFontFamily.helvetica, 7),
        bounds: Rect.fromLTWH(pageSize.width / 3 * 2, pageSize.height - 30,
            pageSize.width / 3, 20),
        format: PdfStringFormat(alignment: PdfTextAlignment.center));
    page.graphics.drawImage(
      image,
      Rect.fromLTWH(
          pageSize.width / 3 * 2, pageSize.height - 70, pageSize.width / 3, 50),
    );
    PdfTextElement(
      format: PdfStringFormat(lineSpacing: 0),
    ).draw(
      page: page,
      bounds: Rect.fromLTWH(
        10,
        350,
        pageSize.width / 3,
        pageSize.height - 100,
      ),
    );
  }
}
