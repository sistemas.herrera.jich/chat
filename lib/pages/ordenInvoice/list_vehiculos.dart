import 'package:chat/models/vehiculo.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:chat/services/auth_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/models/vehiculos.dart';
import 'package:chat/services/vehiculos_services.dart';

class ListVehiculos extends StatefulWidget {
  @override
  _ListVehiculosState createState() => _ListVehiculosState();
}

class _ListVehiculosState extends State<ListVehiculos> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  final vehiculosServices = new GetVehiculos();
  final bool conection = false;
  List<Vehiculo> vehiculos = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    final socketService = Provider.of<SocketServices>(context);
    final status = socketService.serverStatus;
    final usuario = authService.usuario;

    VehiculoUID args = ModalRoute.of(context).settings.arguments;
    setState(() {});
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${usuario.nombre}",
          style: TextStyle(color: Colors.black),
        ),
        elevation: 1,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.exit_to_app,
            color: Colors.black,
          ),
          onPressed: () {
            socketService.disconnect();
            AuthService.deleteToken();

            Navigator.pushReplacementNamed(context, 'login');
          },
        ),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: status == ServerStatus.Online
                ? Icon(
                    Icons.online_prediction,
                    color: Colors.green,
                  )
                : Icon(
                    Icons.offline_bolt,
                    color: Colors.red,
                  ),
          )
        ],
      ),
      body: SmartRefresher(
        controller: _refreshController,
        enablePullDown: true,
        child: _listViewUsuarios(),
        header: WaterDropHeader(
          complete: Icon(
            Icons.check,
            color: Colors.blue[400],
          ),
          waterDropColor: Colors.blue[400],
        ),
        onRefresh: _cargarUsuarios(args.uid),
      ),
    );
  }

  ListView _listViewUsuarios() {
    return ListView.separated(
        physics: BouncingScrollPhysics(),
        itemBuilder: (_, i) => _usuarioListTitle(vehiculos[i]),
        separatorBuilder: (_, i) => Divider(),
        itemCount: vehiculos.length);
  }

  ListTile _usuarioListTitle(Vehiculo vehiculo) {
    return ListTile(
      title: Text(vehiculo.nombre),
      subtitle: Text(vehiculo.fechaIngreso.toString()),
      leading: CircleAvatar(
          child: Text(vehiculo.nombre.substring(0, 2),
              style: TextStyle(color: Colors.red[600])),
          backgroundColor: Colors.black87),
      trailing: Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
            color: vehiculo.etapa == 6 ? Colors.green : Colors.redAccent,
            borderRadius: BorderRadius.circular(100)),
      ),
      onTap: () {
        Navigator.pushNamed(context, 'invoice');
      },
    );
  }

  _cargarUsuarios(uid) async {
    this.vehiculos = await vehiculosServices.getVehiculos(uid);
    setState(() {});
    _refreshController.refreshCompleted();
  }
}
