import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:async';
import 'dart:convert';
import 'package:chat/models/data.dart';
import 'package:chat/widgets/custom_inputs.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';

import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';

import 'package:chat/helpers/mostrarAlertas.dart';
import 'package:chat/services/vehiculos_services.dart';
import 'package:chat/services/upload_service.dart';

import 'list_vehiculos_historial.dart';

class SignatureFinal extends StatefulWidget {
  final Data data;
  const SignatureFinal(this.data);

  @override
  _SignatureFinalState createState() => _SignatureFinalState();
}

class _SignatureFinalState extends State<SignatureFinal> {
  final keySignaturePad = GlobalKey<SfSignaturePadState>();
  final keySignaturePadCliente = GlobalKey<SfSignaturePadState>();
  final vehiculosServices = new GetVehiculos();
  final upload = new Upload();
  TextEditingController servicios = new TextEditingController();
  TextEditingController observaciones = new TextEditingController();

  List<String> ordenesCompra=[];
  TextEditingController orden = TextEditingController();
  void _addOrden(value) {
    setState(() {
      ordenesCompra.add(value);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(148, 19, 5, 1),
        title: Text('Firmar Orden Salida',
            style: TextStyle(color: Color.fromRGBO(3, 3, 3, 1))),
        actions: [
          TextButton(
            onPressed: () async {
              keySignaturePadCliente.currentState.clear();
            },
            child: Icon(Icons.brush_sharp),
            style: TextButton.styleFrom(primary: Color.fromRGBO(3, 3, 3, 1)),
          )
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            color: Color.fromRGBO(116, 125, 144, 1),
            child: Column(
              children: [
                SfSignaturePad(
                  key: keySignaturePadCliente,
                  backgroundColor: Colors.white.withOpacity(0.2),
                ),
                Divider(),
                SizedBox(height: 10),
                CustomInput(
                    controller: servicios,
                    boolean: false,
                    hintText: 'Servicios realizados',
                    icon: Icons.add_alert,
                    inputType: TextInputType.text),
                SizedBox(height: 10),
                CustomInput(
                    controller: observaciones,
                    boolean: false,
                    hintText: 'Observaciones',
                    icon: Icons.add_alert,
                    inputType: TextInputType.text),
                Text(
                  'Listado de ordenes de compra:', style: TextStyle(color: Colors.blueAccent, fontSize: 20),
                ),
                TextFormField(
                  controller: orden,
                  textCapitalization: TextCapitalization.characters,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: 'Introducir orden de compra'
                  ),
                  validator: (String value) {
                    return (value != null) ? 'El campo es requerido' : null;
                  },
                  onChanged: (String value){
                    if(value.endsWith(' ')){
                      _addOrden(value);
                      orden.clear();
                    }
                  },
                  onEditingComplete: (){
                      _addOrden(orden.text);
                      orden.clear();
                  },
                ),
                Expanded(child: ListView.builder(
                  itemCount: ordenesCompra.length,
                  itemBuilder: (context, index) {
                    String item = ordenesCompra[index];
                    return ListTile(
                      title: Text(item),
                    );
                  },
                )),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Color.fromRGBO(148, 19, 5, 1),
          minimumSize: Size(double.infinity, 50),
        ),
        child: Text('Guardar firma',
            style: TextStyle(color: Color.fromRGBO(3, 3, 3, 1))),
        onPressed: () => {onSubmit(widget.data)},
      ),
    );
  }

  Future onSubmit(data) async {
    try {
      final imageCliente = await keySignaturePadCliente.currentState?.toImage();

      ByteData imageSignatureCliente =
          await imageCliente.toByteData(format: ui.ImageByteFormat.png);

      final img64 = base64.encode(imageSignatureCliente.buffer.asUint8List());
      await upload.setUploadFile(data.uidVehiculo, img64, data.uidPdfEntradas,
          servicios.text, observaciones.text);

      Navigator.pushReplacement(context,
          PageRouteBuilder(pageBuilder: (_, __, ___) => HistorialVehiculo()));
    } catch (e) {
      mostrarAlertas(
          context, "Error", 'Escriba la firma antes de continuar $e');
    }
  }
}
