import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import 'package:chat/pages/ordenInvoice/list_vehiculos_historial.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/pages/login.dart';
import 'package:chat/services/socket_service.dart';

class LoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: checkLoginState(context),
        builder: (context, snapshot) {
          return Center(
            child: CircularProgressIndicator(
              strokeWidth: 6,
            ),
          );
        },
      ),
    );
  }

  Future checkLoginState(BuildContext context) async {
    final authService = Provider.of<AuthService>(context, listen: false);
    final socketService = Provider.of<SocketServices>(context);
    final autenticado = await authService.isLoggedIn();

    if (autenticado) {
      socketService.connect();
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacement(context,
            PageRouteBuilder(pageBuilder: (_, __, ___) => HistorialVehiculo()));
      });
    } else {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacement(context,
            PageRouteBuilder(pageBuilder: (_, __, ___) => LoginPage()));
      });
    }
  }
}
