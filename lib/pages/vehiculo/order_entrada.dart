import 'package:chat/models/vehiculo.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:chat/helpers/mostrarAlertas.dart';
import 'package:chat/widgets/boton_azul.dart';
import 'package:chat/widgets/custom_inputs.dart';
import 'package:provider/provider.dart';

class OrdenEntrada extends StatelessWidget {
  const OrdenEntrada({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(116, 125, 144, 1),
        appBar: new AppBar(
          backgroundColor: Color.fromRGBO(148, 19, 5, 1),
          centerTitle: true,
          title: new Text(
            'VEHICULO',
            style: TextStyle(color: Color.fromRGBO(3, 3, 3, 1)),
          ),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.9,
              child: Form(),
            ),
          ),
        ));
  }
}

class Form extends StatefulWidget {
  Form({Key key}) : super(key: key);

  @override
  _FormState createState() => _FormState();
}

class _FormState extends State<Form> {
  final nombre = new TextEditingController();
  final telefono = new TextEditingController();
  final rfc = new TextEditingController();
  final modelo = new TextEditingController();
  final tipo = new TextEditingController();
  final marca = new TextEditingController();
  final color = new TextEditingController();
  final placa = new TextEditingController();
  final nseries = new TextEditingController();
  final nmotor = new TextEditingController();
  final kmrecorridos = new TextEditingController();
  String uidVehiculo = "";
  SocketServices socketService;
  AuthService auhtService;
  String _selectvalue = "FGE170207LU3";
  List<String> items = ['FGE170207LU3', 'XAXX010101000'];

  @override
  void initState() {
    super.initState();
    this.socketService = Provider.of<SocketServices>(context, listen: false);
    this.auhtService = Provider.of<AuthService>(context, listen: false);

    socketService.connect();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 50, left: 20, right: 20),
      child: Column(children: <Widget>[
        CustomInput(
            controller: nombre,
            boolean: false,
            hintText: 'Nombre',
            icon: Icons.person_add,
            inputType: TextInputType.text),
        CustomInput(
            controller: telefono,
            boolean: false,
            hintText: 'Telefono',
            icon: Icons.phone,
            inputType: TextInputType.phone),
        SizedBox(height: 10), //DropdownButton for RFC VALUES
        Container(
          width: 400,
          height: 60,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black.withOpacity(0.05),
                    offset: Offset(0, 5),
                    blurRadius: 5)
              ]),
          child: DropdownButton(
            underline: SizedBox(),
            value: _selectvalue,
            icon: Icon(Icons.keyboard_arrow_down),
            items: items.map((String items) {
              return DropdownMenuItem(
                  value: items,
                  child:
                      Container(width: 300, child: Center(child: Text(items))));
            }).toList(),
            onChanged: (String newValue) {
              setState(() {
                _selectvalue = newValue;
              });
            },
          ),
        ),
        SizedBox(height: 10),
        CustomInput(
            controller: modelo,
            boolean: false,
            hintText: 'Modelo',
            icon: Icons.ac_unit,
            inputType: TextInputType.text),
        CustomInput(
            controller: tipo,
            boolean: false,
            hintText: 'Tipo',
            icon: Icons.ac_unit,
            inputType: TextInputType.text),
        SizedBox(height: 60),
        Container(
          width: MediaQuery.of(context).size.width / 1.5,
          height: MediaQuery.of(context).size.height / 15,
          child: BotonAzul(
            text: "Continuar",
            onPressed: () {
              FocusScope.of(context).unfocus();
              if (nombre.text != "" &&
                  telefono.text != "" &&
                  modelo.text != "" &&
                  tipo.text != "") {
                var object = {
                  'uid': '',
                  'nombre': nombre.text,
                  'telefono': telefono.text,
                  'rfc': _selectvalue,
                  'modelo': modelo.text,
                  'tipo': tipo.text,
                  'usuario': this.auhtService.usuario.uid
                };
                vehiculoAdd(object);
              } else {
                mostrarAlertas(context, 'Registro Incorrecto',
                    'Llene todos los campos para continuar');
              }
            },
          ),
        )
      ]),
    );
  }

  void vehiculoAdd(object) {
    this.socketService.socket.emit('orden-entrada', object);
    this.socketService.socket.on('orden-entrada', _result);
  }

  void _result(dynamic data) {
    if (data['vehiculo']['uid'] != '') {
      setState(() {
        uidVehiculo = data['vehiculo']['uid'];
        print(uidVehiculo);
      });
      Navigator.pushNamed(context, 'ordenentradacontinuacion',
          arguments: VehiculoUID(uidVehiculo));
    }
  }
}
