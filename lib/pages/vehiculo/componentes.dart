import 'package:chat/helpers/colored.dart';
import 'package:chat/models/vehiculo.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/widgets/boton_azul.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class Componentes extends StatefulWidget {
  const Componentes({Key key}) : super(key: key);

  @override
  _ComponentesState createState() => _ComponentesState();
}

class _ComponentesState extends State<Componentes> {
  List<CheckBoxListTileModel> checkBoxListTileModel =
      CheckBoxListTileModel.getUsers();
  SocketServices socketService;
  AuthService auhtService;

  @override
  void initState() {
    super.initState();
    this.socketService = Provider.of<SocketServices>(context, listen: false);
    this.auhtService = Provider.of<AuthService>(context, listen: false);

    socketService.connect();
  }

  @override
  Widget build(BuildContext context) {
    VehiculoUID args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: backGround,
      appBar: AppBar(
        backgroundColor: redPrimary,
        centerTitle: true,
        title: new Text(
          'Componentes',
          style: TextStyle(color: textColor),
        ),
      ),
      body: Card(
        color: backGround,
        child: new Container(
          padding: new EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[0].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[0].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[0].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 0);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[1].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[1].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[1].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 1);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[2].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[2].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[2].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 2);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[3].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[3].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[3].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 3);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[4].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[4].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[4].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 4);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[5].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[5].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[5].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 5);
                  }),
              SizedBox(height: 60),
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: BotonAzul(
                  text: "Continuar",
                  onPressed: () {
                    FocusScope.of(context).unfocus();
                    if (true) {
                      var objects = {
                        'uid': '',
                        'uidVehiculo': args.uid,
                        'claxon': checkBoxListTileModel[0].isCheck,
                        'taponAceite': checkBoxListTileModel[1].isCheck,
                        'taponRadiador': checkBoxListTileModel[2].isCheck,
                        'varillaAceite': checkBoxListTileModel[3].isCheck,
                        'catalizador': checkBoxListTileModel[4].isCheck,
                        'bateria': checkBoxListTileModel[5].isCheck
                      };

                      this
                          .socketService
                          .socket
                          .emit('componentes-entrada', objects);
                      Navigator.pushNamed(context, 'orden-final',
                          arguments: VehiculoUID(args.uid));
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void itemChange(bool val, int index) {
    setState(() {
      checkBoxListTileModel[index].isCheck = val;
    });
  }
}

class CheckBoxListTileModel {
  int userId;
  Icon img;
  String title;
  bool isCheck;

  CheckBoxListTileModel({this.userId, this.img, this.title, this.isCheck});

  static List<CheckBoxListTileModel> getUsers() {
    return <CheckBoxListTileModel>[
      CheckBoxListTileModel(
          userId: 1, img: Icon(Icons.upcoming), title: "Claxon", isCheck: true),
      CheckBoxListTileModel(
          userId: 2,
          img: Icon(Icons.pivot_table_chart_sharp),
          title: "Tapon Aceite",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 3,
          img: Icon(Icons.cancel),
          title: "Tapon de radiador",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 4,
          img: Icon(Icons.add_to_photos),
          title: "Varilla de aceite",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 5,
          img: Icon(Icons.ac_unit_sharp),
          title: "Catalizador",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 6,
          img: Icon(Icons.device_hub),
          title: "Batería (MCA)",
          isCheck: false),
    ];
  }
}
