import 'package:chat/helpers/colored.dart';
import 'package:chat/models/vehiculo.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/widgets/boton_azul.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Accesorios extends StatefulWidget {
  const Accesorios({Key key}) : super(key: key);

  @override
  _AccesoriosState createState() => _AccesoriosState();
}

class _AccesoriosState extends State<Accesorios> {
  List<CheckBoxListTileModel> checkBoxListTileModel =
      CheckBoxListTileModel.getUsers();
  SocketServices socketService;
  AuthService auhtService;

  @override
  void initState() {
    super.initState();
    this.socketService = Provider.of<SocketServices>(context, listen: false);
    this.auhtService = Provider.of<AuthService>(context, listen: false);

    socketService.connect();
  }

  @override
  Widget build(BuildContext context) {
    VehiculoUID args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: backGround,
      appBar: AppBar(
        backgroundColor: redPrimary,
        centerTitle: true,
        title: new Text(
          'Accesorios',
          style: TextStyle(color: textColor),
        ),
      ),
      body: Card(
        color: backGround,
        child: new Container(
          padding: new EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[0].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[0].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[0].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 0);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[1].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[1].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[1].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 1);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[2].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[2].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[2].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 2);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[3].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[3].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[3].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 3);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[4].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[4].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[4].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 4);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[5].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[5].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[5].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 5);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[6].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[6].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[6].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 6);
                  }),
              SizedBox(height: 60),
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: BotonAzul(
                  text: "Continuar",
                  onPressed: () {
                    FocusScope.of(context).unfocus();
                    var objects = {
                      'uid': '',
                      'uidVehiculo': args.uid,
                      'gato': checkBoxListTileModel[0].isCheck,
                      'maneralGato': checkBoxListTileModel[1].isCheck,
                      'llaveRuedas': checkBoxListTileModel[2].isCheck,
                      'estucheHerramientas': checkBoxListTileModel[3].isCheck,
                      'trianguloSeguridad': checkBoxListTileModel[4].isCheck,
                      'llantaRefaccion': checkBoxListTileModel[5].isCheck,
                      'extinguidor': checkBoxListTileModel[6].isCheck
                    };

                    this
                        .socketService
                        .socket
                        .emit('accesorios-entrada', objects);
                    Navigator.pushNamed(context, 'componentes',
                        arguments: VehiculoUID(args.uid));
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void itemChange(bool val, int index) {
    setState(() {
      checkBoxListTileModel[index].isCheck = val;
    });
  }
}

class CheckBoxListTileModel {
  int userId;
  Icon img;
  String title;
  bool isCheck;

  CheckBoxListTileModel({this.userId, this.img, this.title, this.isCheck});

  static List<CheckBoxListTileModel> getUsers() {
    return <CheckBoxListTileModel>[
      CheckBoxListTileModel(
          userId: 1, img: Icon(Icons.upcoming), title: "Gato", isCheck: false),
      CheckBoxListTileModel(
          userId: 2,
          img: Icon(Icons.pivot_table_chart_sharp),
          title: "Maneral de gato",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 3,
          img: Icon(Icons.cancel),
          title: "Llave rudeas",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 4,
          img: Icon(Icons.add_to_photos),
          title: "Estuche herramientas",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 5,
          img: Icon(Icons.ac_unit_sharp),
          title: "Triangulo de seguridad",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 6,
          img: Icon(Icons.device_hub),
          title: "Llanta de refacción",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 7,
          img: Icon(Icons.two_wheeler_rounded),
          title: "Extinguidor",
          isCheck: false),
    ];
  }
}
