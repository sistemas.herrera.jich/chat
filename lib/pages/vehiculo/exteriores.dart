import 'package:chat/helpers/colored.dart';
import 'package:chat/models/vehiculo.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/widgets/boton_azul.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CheckBoxListTileDemo extends StatefulWidget {
  @override
  CheckBoxListTileDemoState createState() => CheckBoxListTileDemoState();
}

class CheckBoxListTileDemoState extends State<CheckBoxListTileDemo> {
  List<CheckBoxListTileModel> checkBoxListTileModel =
      CheckBoxListTileModel.getUsers();
  SocketServices socketService;
  AuthService auhtService;

  @override
  void initState() {
    super.initState();
    this.socketService = Provider.of<SocketServices>(context, listen: false);
    this.auhtService = Provider.of<AuthService>(context, listen: false);

    socketService.connect();
  }

  @override
  Widget build(BuildContext context) {
    VehiculoUID args = ModalRoute.of(context).settings.arguments;
    print(args.uid);
    return Scaffold(
      backgroundColor: backGround,
      appBar: AppBar(
        backgroundColor: redPrimary,
        centerTitle: true,
        title: Text(
          'EXTERIORES',
          style: TextStyle(color: textColor),
        ),
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Card(
          color: backGround,
          child: Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[0].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[0].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[0].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 0);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[1].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[1].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[1].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 1);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[2].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[2].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[2].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 2);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[3].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[3].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[3].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 3);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[4].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[4].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[4].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 4);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[5].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[5].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[5].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 5);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[6].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[6].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[6].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 6);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[7].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[7].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[7].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 7);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[8].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[8].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[8].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 8);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[9].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[9].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[9].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 9);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[10].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[10].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[10].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 10);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[11].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[11].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[11].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 11);
                    }),
                CheckboxListTile(
                    checkColor: backGround,
                    activeColor: bluePrimary,
                    dense: true,
                    //font change
                    title: Text(
                      checkBoxListTileModel[12].title,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5),
                    ),
                    value: checkBoxListTileModel[12].isCheck,
                    secondary: Container(
                      height: 50,
                      width: 50,
                      child: checkBoxListTileModel[12].img,
                    ),
                    onChanged: (bool val) {
                      itemChange(val, 12);
                    }),
                SizedBox(height: 20),
                Container(
                  width: MediaQuery.of(context).size.width / 1.5,
                  //height: MediaQuery.of(context).size.height / 15,
                  child: BotonAzul(
                    text: "Continuar",
                    onPressed: () {
                      FocusScope.of(context).unfocus();
                      if (true) {
                        var objects = {
                          'uid': '', // Cuando actulice se ocupa mandar uid
                          'uidVehiculo': args.uid,
                          'uluces': checkBoxListTileModel[0].isCheck,
                          'cuartoluces': checkBoxListTileModel[1].isCheck,
                          'antena': checkBoxListTileModel[2].isCheck,
                          'espejoLateral': checkBoxListTileModel[3].isCheck,
                          'cristales': checkBoxListTileModel[4].isCheck,
                          'emblema': checkBoxListTileModel[5].isCheck,
                          'llantas': checkBoxListTileModel[6].isCheck,
                          'taponRuedas': checkBoxListTileModel[7].isCheck,
                          'molduras': checkBoxListTileModel[8].isCheck,
                          'taponGasolina': checkBoxListTileModel[9].isCheck,
                          'carroceriaGolpes': checkBoxListTileModel[10].isCheck,
                          'bocinasClaxon': checkBoxListTileModel[11].isCheck,
                          'limpiadores': checkBoxListTileModel[12].isCheck,
                        };

                        this
                            .socketService
                            .socket
                            .emit('exteriores-entrada', objects);
                        Navigator.pushNamed(context, 'interiores',
                            arguments: VehiculoUID(args.uid));
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void itemChange(bool val, int index) {
    setState(() {
      checkBoxListTileModel[index].isCheck = val;
    });
  }
}

class CheckBoxListTileModel {
  int userId;
  Icon img;
  String title;
  bool isCheck;

  CheckBoxListTileModel({this.userId, this.img, this.title, this.isCheck});

  static List<CheckBoxListTileModel> getUsers() {
    return <CheckBoxListTileModel>[
      CheckBoxListTileModel(
          userId: 1,
          img: Icon(Icons.lightbulb),
          title: "Unidad de luces",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 2,
          img: Icon(Icons.lightbulb_outline),
          title: "1/4 Luces",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 3,
          img: Icon(Icons.signal_cellular_alt_outlined),
          title: "Antena",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 4,
          img: Icon(Icons.add_to_photos),
          title: "Espejo Lateral",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 5,
          img: Icon(Icons.ac_unit_sharp),
          title: "Cristales",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 6,
          img: Icon(Icons.device_hub),
          title: "Emblema",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 7, img: Icon(Icons.circle), title: "Llantas", isCheck: false),
      CheckBoxListTileModel(
          userId: 8,
          img: Icon(Icons.circle_outlined),
          title: "Tapon de ruedas",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 9,
          img: Icon(Icons.view_array),
          title: "Molduras completas",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 10,
          img: Icon(Icons.tapas_rounded),
          title: "Tapon de gasolina",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 11,
          img: Icon(Icons.highlight_remove),
          title: "Carroceria sin golpes",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 12,
          img: Icon(Icons.sd_rounded),
          title: "Bocinas de claxon",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 13,
          img: Icon(Icons.cleaning_services),
          title: "Limpiadores (Plumas)",
          isCheck: false),
    ];
  }
}
