import 'package:chat/helpers/colored.dart';
import 'package:chat/models/vehiculo.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:chat/widgets/boton_azul.dart';
import 'package:chat/widgets/custom_inputs.dart';
import 'package:provider/provider.dart';

class Continuacion extends StatelessWidget {
  const Continuacion({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGround,
      appBar: new AppBar(
        backgroundColor: redPrimary,
        centerTitle: true,
        title: new Text(
          'VEHICULO',
          style: TextStyle(color: textColor),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[Form()],
            ),
          ),
        ),
      ),
    );
  }
}

class Form extends StatefulWidget {
  Form({Key key}) : super(key: key);

  @override
  _FormState createState() => _FormState();
}

class _FormState extends State<Form> {
  final nombre = new TextEditingController();
  final telefono = new TextEditingController();
  final rfc = new TextEditingController();
  final modelo = new TextEditingController();
  final tipo = new TextEditingController();
  final marca = new TextEditingController();
  final color = new TextEditingController();
  final placa = new TextEditingController();
  final nseries = new TextEditingController();
  final nmotor = new TextEditingController();
  final kmrecorridos = new TextEditingController();
  SocketServices socketService;
  AuthService auhtService;

  @override
  void initState() {
    super.initState();
    this.socketService = Provider.of<SocketServices>(context, listen: false);
    this.auhtService = Provider.of<AuthService>(context, listen: false);

    socketService.connect();
  }

  @override
  Widget build(BuildContext context) {
    VehiculoUID args = ModalRoute.of(context).settings.arguments;

    return Container(
      margin: EdgeInsets.only(top: 50, left: 20, right: 20),
      child: Column(children: <Widget>[
        CustomInput(
            controller: marca,
            boolean: false,
            hintText: 'Marca',
            icon: Icons.branding_watermark_outlined,
            inputType: TextInputType.text),
        CustomInput(
            controller: color,
            boolean: false,
            hintText: 'Color de la unidad',
            icon: Icons.color_lens,
            inputType: TextInputType.text),
        CustomInput(
            controller: placa,
            boolean: false,
            hintText: 'Placas',
            icon: Icons.confirmation_num,
            inputType: TextInputType.text),
        CustomInput(
            controller: nseries,
            boolean: false,
            hintText: 'Número de Serie',
            icon: Icons.confirmation_num,
            inputType: TextInputType.text),
        CustomInput(
            controller: nmotor,
            boolean: false,
            hintText: 'Número de Motor',
            icon: Icons.confirmation_num,
            inputType: TextInputType.text),
        CustomInput(
            controller: kmrecorridos,
            boolean: false,
            hintText: 'KM Recorridos',
            icon: Icons.speed_outlined,
            inputType: TextInputType.text),
        SizedBox(height: 60),
        Container(
          width: MediaQuery.of(context).size.width / 1.5,
          height: MediaQuery.of(context).size.height / 15,
          child: BotonAzul(
            text: "Continuar",
            onPressed: () {
              FocusScope.of(context).unfocus();
              if (true) {
                var object = {
                  'uid': args.uid,
                  'marca': marca.text,
                  'color': color.text,
                  'placa': placa.text,
                  'nseries': nseries.text,
                  'nmotor': nmotor.text,
                  'kmrecorridos': kmrecorridos.text
                };
                vehiculoAdd(object);
                Navigator.pushNamed(context, 'exteriores',
                    arguments: VehiculoUID(args.uid));
              }
            },
          ),
        )
      ]),
    );
  }

  void vehiculoAdd(object) {
    this.socketService.socket.emit('orden-entrada', object);
  }
}
