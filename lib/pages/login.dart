import 'package:chat/helpers/mostrarAlertas.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/widgets/boton_azul.dart';
import 'package:flutter/material.dart';

import 'package:chat/widgets/custom_inputs.dart';
import 'package:chat/widgets/logo.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(116, 125, 144, 1),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Logo(
                  textLogo: 'TALLER',
                ),
                _Form(),
                /*Labels(
                    textEncabezado: 'No tengo una cuenta',
                    textRoute: 'Crear cuenta!',
                    route: 'register'),*/
                Text(
                  "Términos y condiciones de uso",
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Form extends StatefulWidget {
  _Form({Key key}) : super(key: key);

  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final email = new TextEditingController();
  final password = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    final socketService = Provider.of<SocketServices>(context);
    return Container(
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: <Widget>[
          CustomInput(
              inputType: TextInputType.emailAddress,
              controller: email,
              icon: Icons.mail_outline,
              boolean: false,
              hintText: 'Email'),
          CustomInput(
              inputType: TextInputType.visiblePassword,
              controller: password,
              icon: Icons.accessibility_new_outlined,
              boolean: true,
              hintText: 'Contraseña'),
          SizedBox(
            height: 20,
          ),
          BotonAzul(
            text: "Entrar",
            onPressed: authService.autenticando
                ? null
                : () async {
                    FocusScope.of(context).unfocus();
                    final loginOk = await authService.login(
                        email.text.trim(), password.text.trim());

                    if (loginOk) {
                      socketService.connect();
                      Navigator.pushReplacementNamed(
                          context, 'historial'); //orden-entrada
                    } else {
                      mostrarAlertas(context, 'Login Incorrecto',
                          'Revice sus credenciales');
                    }
                  },
          )
        ],
      ),
    );
  }
}
