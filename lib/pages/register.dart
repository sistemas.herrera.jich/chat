import 'package:chat/models/sucursales.dart';
import 'package:chat/widgets/boton_azul.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:chat/widgets/custom_inputs.dart';
import 'package:chat/widgets/labels.dart';

import 'package:chat/helpers/mostrarAlertas.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/services/sucursales_services.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _Form(),
                Labels(
                    textEncabezado: 'No tengo nada que hacer aquí',
                    textRoute: 'Regresar',
                    route: 'historial'),
                Text(
                  "Términos y condiciones de uso",
                  style: TextStyle(fontWeight: FontWeight.w200),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Form extends StatefulWidget {
  _Form({Key key}) : super(key: key);

  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final email = new TextEditingController();
  final password = new TextEditingController();
  final name = new TextEditingController();

  final sucursalServices = new SucursalServices();
  String _selectvalue = '';
  //List<String> items = ['FGE170207LU3', 'XAXX010101000'];

  List<Sucursal> items = [];
  @override
  void initState() {
    _getSucursales();
    super.initState();
  }

  Map<String, String> itemsopc = Map();

  void pintarSucursales() {
    for (var i = 0; i < items.length; i++) {
      itemsopc[items[i].uid] = items[i].nombre;
    }

    _selectvalue = itemsopc[items[0].uid];
  }

  String obteneruidSucursal() {
    String uidSucursal =
        itemsopc.keys.firstWhere((K) => itemsopc[K] == _selectvalue);
    return uidSucursal;
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    return Container(
      margin: EdgeInsets.only(top: 80),
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: <Widget>[
          Center(child: Text("Seleccione sucursal...")),
          Container(
            width: 400,
            height: 60,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black.withOpacity(0.05),
                      offset: Offset(0, 5),
                      blurRadius: 5)
                ]),
            child: Center(
              child: DropdownButton<String>(
                value: _selectvalue,
                icon: const Icon(Icons.arrow_downward),
                iconSize: 24,
                elevation: 16,
                style: const TextStyle(color: Colors.deepPurple),
                underline: Container(
                  height: 2,
                  color: Colors.deepPurpleAccent,
                ),
                onChanged: (String newValue) {
                  setState(() {
                    _selectvalue = newValue;
                  });
                },
                items: itemsopc.values
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
          ),
          SizedBox(height: 10),
          CustomInput(
              inputType: TextInputType.text,
              controller: name,
              icon: Icons.perm_identity_outlined,
              boolean: false,
              hintText: 'Nombre'),
          CustomInput(
              inputType: TextInputType.emailAddress,
              controller: email,
              icon: Icons.mail_outline,
              boolean: false,
              hintText: 'Email'),
          CustomInput(
              inputType: TextInputType.visiblePassword,
              controller: password,
              icon: Icons.accessibility_new_outlined,
              boolean: true,
              hintText: 'Contraseña'),
          SizedBox(
            height: 20,
          ),
          BotonAzul(
            text: "Crear nuevo",
            onPressed: authService.autenticando
                ? null
                : () async {
                    var sucursaluid = obteneruidSucursal();

                    FocusScope.of(context).unfocus();
                    final resgiterOk = await authService.register(
                        name.text.toString(),
                        email.text.trim(),
                        password.text.trim(),
                        sucursaluid);
                    if (resgiterOk == true) {
                      Navigator.of(context).pop();
                    } else {
                      mostrarAlertas(
                          context, 'Registro Incorrecto', resgiterOk);
                    }
                  },
          )
        ],
      ),
    );
  }

  _getSucursales() async {
    this.items = await sucursalServices.getSucursal();
    pintarSucursales();
    setState(() {});
  }
}
