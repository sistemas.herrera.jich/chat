import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  final String textLogo;

  const Logo({Key key, @required this.textLogo}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 400,
        decoration: BoxDecoration(
            color: Color.fromRGBO(148, 19, 5, 1), shape: BoxShape.rectangle),
        margin: EdgeInsets.only(top: 50),
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Image(
                  image: AssetImage('assets/images/tag-logo.png'), width: 400),
              SizedBox(
                height: 0,
              ),
              Text(this.textLogo,
                  style: TextStyle(
                      fontSize: 30,
                      color: Color.fromRGBO(3, 3, 3, 1),
                      fontFamily: ''))
            ],
          ),
        ),
      ),
    );
  }
}
