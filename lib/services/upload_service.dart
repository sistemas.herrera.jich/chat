import 'package:http/http.dart' as http;

import 'package:chat/global/enviroment.dart';
import 'package:chat/services/auth_services.dart';
import 'dart:convert';

class Upload {
  Future<int> setUploadFile(uid, file, opc, servicio, observaciones) async {
    try {
      var data = jsonEncode({
        'uid': uid,
        'opc': opc,
        'filename': file,
        'servicios': servicio,
        'observaciones': observaciones
      });

      final url = Uri.parse('${Enviroments.apiUrl}/vehiculos/upload');
      Map<String, String> headers = {
        "Content-type": 'application/json; charset=UTF-8',
        "x-token": await AuthService.getToken()
      };
      final resp = await http.post(url, headers: headers, body: data);

      return resp.statusCode;
    } catch (e) {
      return 0;
    }
  }
}
