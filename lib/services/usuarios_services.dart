import 'package:http/http.dart' as http;

import 'package:chat/models/usuario.dart';
import 'package:chat/models/usuarios.dart';
import 'package:chat/global/enviroment.dart';
import 'package:chat/services/auth_services.dart';

class UsuariosServices {
  Future<List<Usuario>> getUsuarios() async {
    try {
      final url = Uri.parse('${Enviroments.apiUrl}/usuarios');
      Map<String, String> headers = {
        "Content-type": "application/json",
        "x-token": await AuthService.getToken()
      };
      final resp = await http.get(url, headers: headers);

      final usuarios = usuariosFromJson(resp.body);

      return usuarios.usuarios;
    } catch (e) {
      return [];
    }
  }
}
