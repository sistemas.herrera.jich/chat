import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:chat/pages/chat.dart';
import 'package:chat/pages/loading.dart';
import 'package:chat/pages/login.dart';
import 'package:chat/pages/register.dart';
import 'package:chat/pages/usuarios.dart';
import 'package:chat/pages/vehiculo/order_entrada.dart';
import 'package:chat/pages/vehiculo/orden_entrada_continuacion.dart';
import 'package:chat/pages/vehiculo/exteriores.dart';
import 'package:chat/pages/ordenInvoice/list_vehiculos.dart';
import 'package:chat/pages/vehiculo/accesorios.dart';
import 'package:chat/pages/vehiculo/componentes.dart';
import 'package:chat/pages/vehiculo/final_orden.dart';
import 'package:chat/pages/vehiculo/interiores.dart';
import 'package:chat/pages/ordenInvoice/signature.dart';
import 'package:chat/pages/ordenInvoice/list_vehiculos_historial.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'login': (_) => LoginPage(),
  'loading': (_) => LoadingPage(),
  'usuarios': (_) => UsuariosPage(),
  'register': (_) => RegisterPage(),
  'chat': (_) => ChatPage(),
  'orden-entrada': (_) => OrdenEntrada(),
  'ordenentradacontinuacion': (_) => Continuacion(),
  'exteriores': (_) => CheckBoxListTileDemo(),
  'interiores': (_) => Interiores(),
  'accesorios': (_) => Accesorios(),
  'componentes': (_) => Componentes(),
  'orden-final': (_) => OrdenServicio(),
  'vehiculos-listen': (_) => ListVehiculos(),
  'signature': (_) => SignaturePage(),
  'historial': (_) => HistorialVehiculo()
};
